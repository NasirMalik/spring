package com.nasir;

import com.nasir.model.Address;
import com.nasir.model.Department;
import com.nasir.model.User;
import com.nasir.service.AddressService;
import com.nasir.service.BaseService;
import com.nasir.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

/**
 * Created by nasir on 18/5/19.
 */

public class Runner {

    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.testXMLConfig();
        runner.testAllConfigs();
        runner.testAutoWiring();
    }

    /*
    By XML
     */
    public void testXMLConfig() {
        System.out.println("Start Loading");
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        System.out.println("End Loading");
        User user1 = (User) ctx.getBean(User.class);
        System.out.println("User1: " + user1);

        User user2 = (User) ctx.getBean("userBean");
        System.out.println("User2: " + user2);

        User user3 = new User();
        System.out.println("User3: " + user3);

        Department department = (Department) ctx.getBean("department");
        System.out.println(department);

        Assert.isTrue(user1 == user2);
        Assert.isTrue(user1 != user3);
        Assert.isTrue(user2 != user3);
    }

    public void testAutoWiring() {
        AnnotationConfigApplicationContext annot_ctx = new AnnotationConfigApplicationContext(Config.class);
        // annot_ctx.scan("com.nasir");
        // OR
        // annot_ctx.register(Config.class);

        // If register now
        // annot_ctx.refresh();

//        BaseService service = (BaseService) annot_ctx.getBean("userService");
//        service.getAll();

        User user = annot_ctx.getBean(User.class);
        System.out.println(user);

        Address address = annot_ctx.getBean(Address.class);
        System.out.println(address);
    }

    /*
    All Configs combined
     */
    public void testAllConfigs() {
        AnnotationConfigApplicationContext annot_ctx = new AnnotationConfigApplicationContext();
        // annot_ctx.scan("com.nasir");
        // OR
        annot_ctx.register(Config.class);
        annot_ctx.refresh();

        User user = (User) annot_ctx.getBean("userBean");
        System.out.println(user);

        User user2 = (User) annot_ctx.getBean("userBean");
        System.out.println(user2);

        User user3 = new User();
        System.out.println(user3);

        Assert.isTrue(user == user2);

        Department dept = (Department) annot_ctx.getBean("department");
        System.out.println(dept);

        BaseService service = (BaseService) annot_ctx.getBean("userService");
        service.getAll();

        service = (BaseService) annot_ctx.getBean("addressService");
        service.getAll();
    }

}
