package com.nasir.service;

import com.nasir.dao.BaseDao;
import com.nasir.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by nasir on 18/5/19.
 */
@Component
@Qualifier("userService")
public class UserService extends BaseService {

    @Autowired
//    @Qualifier("userDao")
    private BaseDao userDao;

    @Override
    public User get(Integer userId) {
        System.out.println("In getUser of UserService");
        userDao.get(userId);
        return null;
    }

    @Override
    public List<Object> getAll() {
        System.out.println("In getAll of UserService");
        userDao.getAll();
        return null;
    }

}
