package com.nasir.service;

import com.nasir.model.User;

import java.util.List;

/**
 * Created by nasir on 18/5/19.
 */
public abstract class BaseService {

    public abstract Object get(Integer id);
    public abstract List<Object> getAll();

}
