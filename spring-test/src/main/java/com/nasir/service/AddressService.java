package com.nasir.service;

import com.nasir.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by nasir on 18/5/19.
 */
@Component
@Qualifier("addressService")
public class AddressService extends BaseService {

    @Override
    public User get(Integer userId) {
        System.out.println("In getUser of AddressService");
        return null;
    }

    @Override
    public List<Object> getAll() {
        System.out.println("In getAll of AddressService");
        return null;
    }
}
