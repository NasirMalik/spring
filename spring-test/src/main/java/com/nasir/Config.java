package com.nasir;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by nasir on 18/5/19.
 */
@Configuration
@ComponentScan("com.nasir")
@ImportResource("beans.xml")
public class Config {
}
