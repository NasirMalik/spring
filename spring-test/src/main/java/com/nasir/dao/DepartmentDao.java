package com.nasir.dao;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by nasir on 19/5/19.
 */
@Component
@Qualifier("departmentDao")
public class DepartmentDao extends BaseDao {

    @Override
    public Object get(Integer id) {
        System.out.println("from get in DepartmentDao");
        return null;
    }

    @Override
    public List<Object> getAll() {
        System.out.println("from getAll in DepartmentDao");
        return null;
    }

}
