package com.nasir.dao;

import java.util.List;

/**
 * Created by nasir on 19/5/19.
 */
public abstract class BaseDao {

    public abstract Object get(Integer id);
    public abstract List<Object> getAll();

}
