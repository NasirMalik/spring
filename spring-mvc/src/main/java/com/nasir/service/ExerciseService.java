package com.nasir.service;

import java.util.List;

import com.nasir.model.Activity;

public interface ExerciseService {

	List<Activity> findAllActivities();

}