package com.nasir.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ResourceBundle;

@Controller
public class HelloController {

	@RequestMapping(value ="/greeting")
	public String sayHello (Model model) {
		
		model.addAttribute("greeting", ResourceBundle.getBundle("messages").getObject("test.message").toString()); //"Hello World");
		
		return "hello";
	}
	
}
