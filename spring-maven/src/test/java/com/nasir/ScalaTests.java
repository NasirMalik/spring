package com.nasir;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ScalaTests {

    @Autowired
    private Hello hello;

    @Autowired
    private ScalaClass scalaClass;

    @Test
    public void testScalaClass1Arg(){
        ScalaClass scalaClass = new ScalaClass(true);
        Assert.assertEquals("defaultString : -1".toUpperCase(), scalaClass.printdefaults());
        Assert.assertEquals(12, scalaClass.plus(1, 11));
    }

    @Test
    public void testScalaClassNoArg(){
        Assert.assertEquals("null : -1".toUpperCase(), scalaClass.printdefaults());
        Assert.assertEquals(12, scalaClass.plus(1, 11));
    }

    @Test
    public void testHello(){
        hello.printIt("Saying Hello from Test");
    }

}
