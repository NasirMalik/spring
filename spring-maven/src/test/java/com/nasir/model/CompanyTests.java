package com.nasir.model;

import com.nasir.Application;
import com.nasir.repo.CompanyRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class CompanyTests {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CompanyRepository companyRepository;

    @Test
    public void testExisitngCompany(){
        Assert.assertEquals("Steve's Family Car Centre",
                companyRepository.findAll()
                    .stream()
                    .map(Company::getName)
                    .collect(Collectors.joining(", ")));
    }

    @Test
    public void shouldTestLogging() {
        logger.info("Testing logging");
    }
}
