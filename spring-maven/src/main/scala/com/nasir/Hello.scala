package com.nasir

import org.springframework.stereotype.Component

@Component
final class Hello {
  def printIt(a: String)  = println(a)
  printIt("Testing 123")
}