package com.nasir

import org.springframework.stereotype.Component

@Component
final class ScalaClass(aString: String, val anInteger: Int) {

  def this() {this("defaultString", -1)}

  def this(aBool: Boolean) {
    this("defaultString", -1)
  }

  def printdefaults(): String = if (aString == null) "No Defaults"
                                else (aString ++ " : " ++ anInteger.toString).toUpperCase

  val theString = "theString"

  var someString = "some"

  def plus(x: Int, y: Int): Int = x + y

}
