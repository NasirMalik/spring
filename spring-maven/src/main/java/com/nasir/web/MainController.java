package com.nasir.web;

import com.nasir.ScalaClass;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @RequestMapping("/add/{first}/{second}")
    public Integer addInts(@PathVariable Integer first, @PathVariable Integer second) {
        return new ScalaClass(true).plus(first, second);
    }

}
